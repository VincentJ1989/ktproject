package com.example.ktproject.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ktproject.R
import com.example.ktproject.adapter.UserItemAdapter
import com.example.ktproject.entity.UserInfo
import com.example.ktproject.ext.toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mUserAdapter: UserItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initData()
    }

    private fun initView() {
        main_user_rv.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            mUserAdapter = UserItemAdapter(R.layout.item_user)
            mUserAdapter.apply {
                adapter = this
                setOnItemClickListener { _, _, position ->
                    toast("点击了${data[position].userName}")
                }
            }

        }
    }

    private fun initData() {
        val userInfos = arrayListOf(
            UserInfo("名字1", "签名1"),
            UserInfo("名字2", "签名2"),
            UserInfo("名字3", "签名3"),
            UserInfo("名字4", "签名4"),
            UserInfo("名字5", "签名5"),
            UserInfo("名字6", "签名6"),
            UserInfo("名字7", "签名7")
        )
        mUserAdapter.data = userInfos
    }

}
