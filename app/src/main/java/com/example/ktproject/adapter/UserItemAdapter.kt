package com.example.ktproject.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.ktproject.R
import com.example.ktproject.entity.UserInfo

class UserItemAdapter(layoutResId: Int) : BaseQuickAdapter<UserInfo, BaseViewHolder>(layoutResId) {
    override fun convert(helper: BaseViewHolder, item: UserInfo?) {
        item?.apply {
            helper.setText(R.id.user_name_tv, userName)
                .setText(R.id.user_sign_tv, userSign)
        }
    }
}